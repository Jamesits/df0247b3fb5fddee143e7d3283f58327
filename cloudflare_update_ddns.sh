#!/bin/bash
set -eu

# account config
AUTH_EMAIL="your-email@domain.tld"
AUTH_KEY="xxxxxxxxxxxxxxxxxxxxxxxxx"
ZONE_IDENTIFIER="yyyyyyyyyyyyyyyyyyyyyyyyyy"
RECORD_IDENTIFIER="zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"

# record config
TYPE="A"
NAME="your.domain.tld"
CONTENT=$(curl -fsSL4 ip.sb)
# Time to live for DNS record. Value of 1 is 'automatic'
TTL=120
# Whether the record is receiving the performance and security benefits of Cloudflare
PROXIED="false"

echo -e "Updating record: ${NAME}\t${TYPE}\t${CONTENT}"

RESULT=$(curl -fsSL -X PUT "https://api.cloudflare.com/client/v4/zones/${ZONE_IDENTIFIER}/dns_records/${RECORD_IDENTIFIER}" \
     -H "X-Auth-Email: ${AUTH_EMAIL}" \
     -H "X-Auth-Key: ${AUTH_KEY}" \
     -H "Content-Type: application/json" \
     --data "{\"type\":\"${TYPE}\",\"name\":\"${NAME}\",\"content\":\"${CONTENT}\",\"ttl\":${TTL},\"proxied\":${PROXIED}}")

echo "Got result: ${RESULT}"

if grep '"success":true' <<< ${RESULT} >/dev/null ;then
        echo "The result is positive."
        exit 0;
else
        echo "The result is negative."
        exit 1;
fi